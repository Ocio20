<?php

require_once($rutaBase."/php/config.php");
class BD
{
	public $host;
	private $nomBD;
	private $usuario;
	private $pass;
	
	private $conexionBD;


	function BD($nombreBD)
	{
		global $hostname;
		global $username;
		global $password;
		$this->host = $hostname;
		$this->nomBD = $nombreBD;
		$this->usuario = $username;
		$this->pass = $password;
		
	}
	
	
	function conectar()
	{
		//Preconectamos a la base de datos
		$this->conexionBD = mysql_pconnect($this->host, $this->usuario, $this->pass) or die(mysql_error());
		//Elegimos la base de datos
		mysql_select_db($this->nomBD, $this->conexionBD);

	}
	
	function desconectar()
	{
		mysql_close($this->conexionBD);
	}
	
	function preguntarBD($pregunta)
	{
		//Mostramos consulta
		//echo $pregunta."<br>";
		
		//Preguntamos a la base de datos
		$retorno = mysql_query($pregunta, $this->conexionBD) or die(mysql_error());
		return ($retorno);
	}
	
	function numResult($result)
	{
		return (mysql_num_rows($result));
	}

}//BD

?>