/**
 * 
 */
package com.Ocio20.ui.forms.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;

/**
 * @author dani
 *
 */
public class Peticion {

	//Variables
	private RequestBuilder consultor;
	private String respuestaJSON;


	
	//Clases Auxiliares
	private class JSONRespuesta implements RequestCallback {
	    public void onError(Request request, Throwable ex) {
			//log the error
			GWT.log( "Error PeticionPOST ", ex );
	    }

	    public void onResponseReceived(Request request, Response response) {

	    	if (response.getStatusCode() != 200) {
	    		Window.alert("Sorry, there was an error...");
	    		return;
	    		}
		    
	    	try {

		    	respuestaJSON =  response.getText();

		    	String textoRespuesta = response.getText();
		    	TextArea div = new TextArea();
		    	div.setText(respuestaJSON.toString());
		    	RootPanel.get().add(div);
		    	
	

	    	} catch (Exception e) {
				//log the error
				GWT.log( "Error PeticionPOST parse" + response.getText(),  e );
			}
	    }
	}
	
	

	
	
	
	/**
	 * Método que creara la conexión al php, con el método POST de los formularios
	 * @param urlScript - URL del archivo php a consultar (desde el directorio raíz)
	 * @param tipo - tipo de envio de la petición <b>"POST"</b> o <b>"GET"</b>
	 * @author Dani
	 */
	public Peticion(String urlScript, String tipo) {
		
		if (tipo=="POST"||tipo=="post")
			consultor = new RequestBuilder(RequestBuilder.POST, urlScript);
		if (tipo=="GET"||tipo=="get")
			consultor = new RequestBuilder(RequestBuilder.GET, urlScript);
	}

	
	/**
	 * Metodo para realizar una peticion JSON al servidor, es necesario especificar 
	 * con uno de los <b>constructores</b> el tipo de envío y la ruta.
	 * 
	 * @param datos JSON a enviar a la petición del servidor
	 * @return objeto JSON del servidor
	 */
	public String consultaJSON(JSONObject datos)
	{
	
		try{
			
			
			consultor.sendRequest(datos.toString(), new JSONRespuesta());

			GWT.log( "PeticionPOST consultaJSON 1:" + datos.toString() + " " + respuestaJSON,null);
			
			
			return respuestaJSON;

			
		}
		catch (RequestException e) {
			GWT.log("Could not send search request", e);
			GWT.log( "PeticionPOST consultaJSON ", e);
		}
		return respuestaJSON;
	}





}
