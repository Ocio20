package com.Ocio20.ui.forms.client;

ESTE es un texto que he puesto para probar el git, con esto va a petar el programa, por supuesto

import java.util.Date;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ChangeListener;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * Clase que implementa el formulario de registro de usuarios
 * Utiliza los estilos:
 * .flogin
 * .lreg
 * .txtreg
 * .ereg
 * .okreg
 */
public class FormRegistro implements EntryPoint {
	
	final FormPanel formulario = new FormPanel();
	private FlowPanel fLogin = new FlowPanel();

	//Etiquetas del formulario
	private Label lUsuario = new Label();
	private Label lNombre = new Label();
	private Label lApellidos = new Label();
	private Label lMail = new Label();
	private Label lPass = new Label();
	private Label lPass2 = new Label();
	private Label lSexo = new Label();
	private Label lFecha = new Label();
	
	//Elementos del formulario
	private TextBox txtUsuario = new TextBox();
	private TextBox txtNombre = new TextBox();
	private TextBox txtApellidos = new TextBox();
	private TextBox txtMail = new TextBox();
	private PasswordTextBox ptxtPass = new PasswordTextBox();
	private PasswordTextBox ptxtPass2 = new PasswordTextBox();
	private ListBox lbSexo = new ListBox();
	private ListBox lbDia = new ListBox();
	private ListBox lbMes = new ListBox();
	private ListBox lbAnyo = new ListBox();
	private Date fechaHoy = new Date();
	private Button bEnviar = new Button();
	
	//Campos de error del formulario
	private Label eUsuario = new Label();
	private Label eNombre = new Label();
	private Label eApellidos = new Label();
	private Label eMail = new Label();
	private Label ePass2 = new Label();
	private Label eSexo = new Label();
	private Label eFecha = new Label();




	public void onModuleLoad() {
		  
	    //Añadimos el comportamiento al form
		formulario.setEncoding(FormPanel.ENCODING_MULTIPART);
		formulario.setMethod(FormPanel.METHOD_POST);
		formulario.setAction(GWT.getModuleBaseURL()+"php/forms/regUsu.php");
		formulario.add(fLogin);

		
		
		//ESTILOS
		//Creo que es más cómodo meter los estilos de cada parte juntos, y el resto de comportamiento ya luego, pero bueno, estoy probando
		//panel principal
		fLogin.setStyleName("flogin");
		//labels
		lUsuario.setStyleName("lreg");
		lNombre.setStyleName("lreg");
		lApellidos.setStyleName("lreg");
		lMail.setStyleName("lreg");
		lPass.setStyleName("lreg");
		lPass2.setStyleName("lreg");
		lSexo.setStyleName("lreg");
		lFecha.setStyleName("lreg");
		//textbox y password
		txtUsuario.setStyleName("txtreg");
		txtNombre.setStyleName("txtreg");
		txtApellidos.setStyleName("txtreg");
		txtMail.setStyleName("txtreg");
		ptxtPass.setStyleName("txtreg");
		ptxtPass2.setStyleName("txtreg");
		

		//Introducimos el texto de las labels
		lUsuario.setText("Nick: ");
		lNombre.setText("Nombre: ");
		lApellidos.setText("Apellidos: ");
		lMail.setText("Tu correo electrónico: ");
		lPass.setText("Contraseña nueva: ");
		lPass2.setText("Repetir contraseña: ");
		lSexo.setText("Sexo: ");
		lFecha.setText("Fecha de nacimiento: ");
		bEnviar.setText("Enviar");

		
		//Nombre de cada uno de los campos
		txtUsuario.setName("usuario");
		txtNombre.setName("nombre");
		txtApellidos.setName("apellidos");
		txtMail.setName("mail");
		ptxtPass.setName("pass");
		ptxtPass2.setName("pass2");
		lbSexo.setName("sexo");
		lbDia.setName("dia");
		lbMes.setName("mes");
		lbAnyo.setName("anyo");
		

		//Listas
		lbSexo.addItem("Selecciona el sexo","-1");
		lbSexo.addItem("Femenino", "1");
		lbSexo.addItem("Masculino","2");

		//Dia
		for (Integer i=1; i < 31; i++)
			lbDia.addItem(i.toString(),i.toString());
		//Mes
		lbMes.addItem("Enero","1");
		lbMes.addItem("Febrero","2");
		lbMes.addItem("Marzo","3");
		lbMes.addItem("Abril","4");
		lbMes.addItem("Mayo","5");
		lbMes.addItem("Junio","6");
		lbMes.addItem("Julio","7");
		lbMes.addItem("Agosto","8");
		lbMes.addItem("Septiembre","9");
		lbMes.addItem("Octubre","10");
		lbMes.addItem("Noviembre","11");
		lbMes.addItem("Diciembre","12");
		
		
		//Anyo
		for (Integer i=fechaHoy.getYear()+1900; i >= 1900; i--)
			lbAnyo.addItem(i.toString(),i.toString());
	  

		//EVENTOS DEL FORMULARIO
		//Introducimos los eventos del formulario
		
		//Evento para el txtbox del usuario
		txtUsuario.addChangeListener(new ChangeListener(){
			public void onChange(Widget sender){
				validaUsuario();
			}
		});
		
		
		txtNombre.addChangeListener(new ChangeListener(){
			public void onChange(Widget sender){
				if (!longValida(txtNombre.getText(),4,30))
				{
					eNombre.setStyleName("ereg");
					eNombre.setText("La longitud tiene estar entre 4 y 30 caracteres");
				}
				else
				{
					eNombre.setStyleName("okreg");
					eNombre.setText("Correcto");
				}
			}
		});

		txtApellidos.addChangeListener(new ChangeListener(){
			public void onChange(Widget sender){
				if (!longValida(txtApellidos.getText(),4,30))
				{
					eApellidos.setStyleName("ereg");
					eApellidos.setText("La longitud tiene estar entre 4 y 30 caracteres");
				}
				else
				{
					eApellidos.setStyleName("okreg");
					eApellidos.setText("Correcto");
				}
			}
		});

		txtMail.addChangeListener(new ChangeListener(){
			public void onChange(Widget sender){
				
				//Comprobamos si es un mail válido
				if (!txtMail.getText().matches("(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))
				{
					eMail.setStyleName("ereg");
					eMail.setText("Dirección de correo electrónico incorrecta");
				}
				else
				{
					eMail.setStyleName("okreg");
					eMail.setText("Correcto");
				}
			}
		});
		
		ChangeListener pass = new ChangeListener(){
			public void onChange(Widget sender){
				
				//Longitud de la contraseña entre 4 y 20
				if (!longValida(ptxtPass.getText(),4,20) || !longValida(ptxtPass2.getText(),4,20))
				{
					ePass2.setStyleName("ereg");
					ePass2.setText("La longitud tiene estar entre 4 y 20 caracteres");
				}
				else
					if (!ptxtPass.getText().equals(ptxtPass2.getText()))
					{
						ePass2.setStyleName("ereg");
						ePass2.setText("Las contraseñas introducidas no son iguales");
					}
					else
					{
						ePass2.setStyleName("okreg");
						ePass2.setText("Correcto");
					}
			}
		};
		ptxtPass.addChangeListener(pass);
		ptxtPass2.addChangeListener(pass);
	
		
		
		
		//Evento al hacer clic en el botón de enviar el formulario
		bEnviar.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				formulario.submit();
			}
		});
		
		
		
		
		
		//Metemos los campos del formulario
		fLogin.add(lUsuario);
		fLogin.add(txtUsuario);
		fLogin.add(eUsuario);
		fLogin.add(lNombre);
		fLogin.add(txtNombre);
		fLogin.add(eNombre);
		fLogin.add(lApellidos);
		fLogin.add(txtApellidos);
		fLogin.add(eApellidos);
		fLogin.add(lMail);
		fLogin.add(txtMail);
		fLogin.add(eMail);
		fLogin.add(lPass);
		fLogin.add(ptxtPass);
		fLogin.add(lPass2);
		fLogin.add(ptxtPass2);
		fLogin.add(ePass2);
		fLogin.add(lbSexo);
		fLogin.add(eSexo);
		fLogin.add(lFecha);
		fLogin.add(lbDia);
		fLogin.add(lbMes);
		fLogin.add(lbAnyo);
		fLogin.add(eFecha);
		fLogin.add(bEnviar);
		
		

		//Manejador del formulario para validar justo antes de enviar el formulario
	    formulario.addFormHandler(new FormHandler() {
	      public void onSubmit(FormSubmitEvent event) {
	        // This event is fired just before the form is submitted. We can take
	        // this opportunity to perform validation.
	    	  GWT.log("aaa", null);
	        if (!validaFormulario()) {
	          event.setCancelled(true);
	        }
	      }

	      public void onSubmitComplete(FormSubmitCompleteEvent event) {
	        // When the form submission is successfully completed, this event is
	        // fired. Assuming the service returned a response of type text/html,
	        // we can get the result text here (see the FormPanel documentation for
	        // further explanation).
	    	  GWT.log("bbb", null);
	        Window.alert(event.getResults());
	      }
	    });
		
		//Panel principal del formulario
		RootPanel.get().add(formulario);
	}

		
	
	private void validaUsuario()
	{
		JSONObject datos = new JSONObject();
		String stringRespuesta;

		if (txtUsuario.getText().length()>0);
		{

			Peticion peticion = new Peticion(GWT.getHostPageBaseURL()+"php/forms/petUsuVal.php", "POST");
			
			try
			{
				datos.put("nombre", new JSONString(txtUsuario.getText()));
				stringRespuesta = peticion.consultaJSON(datos);

				//JSONValue retorno = JSONParser.parse(stringRespuesta.toString());
				
				

				//eUsuario.setText(retorno.toString());
				
			}catch(Exception e)
			{
				GWT.log("ValidaUsuario() -> ", e);
			}
		}
	}
	
	
	/**
	 * Comprueba que la longitud de texto esté comprendida entre min y max (incluidos)
	 * @param texto
	 * @param min
	 * @param max
	 * @return
	 */
	private boolean longValida(String texto, int min, int max)
	{
		if (texto.length() >= min && texto.length() <= max)
			return true;
		return false;
	}
	
	
	private boolean validaFormulario()
	{

		//No podemos aceptar << porque peta al enviarlo por confundir etiquetas html
		return(false);
	}


	

}
