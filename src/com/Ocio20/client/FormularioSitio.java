package com.Ocio20.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.StackPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class FormularioSitio implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final Button sendButton = new Button("Send");
		final TextBox nameField = new TextBox();
		nameField.setText("GWT User");

		// We can add style names to widgets
		sendButton.addStyleName("sendButton");

		// Add the nameField and sendButton to the RootPanel
		// Use RootPanel.get() to get the entire body element
		RootPanel.get("nameFieldContainer").add(nameField);
		RootPanel.get("sendButtonContainer").add(sendButton);

//----------------------------------------------------------------------------------------------------------------
		
		/* Datos basicos 
		 	nombre
			direccion	
			tipo de sitio (restaurante, pub, heladeria, bar, cafe, ...) TODO: metemos un textbox con busqueda de los ya definidos -> 
																			  rellenamos una variable en js que tenga la lista y ahí buscamos.
			geolocalizacion -> eso en el mapa!
			
		 */

		// CONSTANTES
		final int MAX_LENGTH_NOMBRE = 35;
		final int VISIBLE_LENGTH_NOMBRE = 35;
		final int MAX_LENGTH_DIRECCION = 35;
		final int VISIBLE_LENGTH_DIRECCION = 35;
		final int MAX_LENGTH_TIPO = 35;
		final int VISIBLE_LENGTH_TIPO = 35;
		//
		
		//final VerticalPanel vpDatosBasicos = new VerticalPanel();
		final Grid gDatosBasicos = new Grid(3, 2);

		final TextBox txtNombre = new TextBox();
		final Label lNombre = new Label("Nombre: ");
		txtNombre.setMaxLength(MAX_LENGTH_NOMBRE);
		txtNombre.setVisibleLength(VISIBLE_LENGTH_NOMBRE);
		gDatosBasicos.setWidget(0, 0, lNombre);
		gDatosBasicos.setWidget(0, 1, txtNombre);
		
		final TextBox txtDireccion = new TextBox();
		final Label lDireccion = new Label("Direccion: ");		
		txtDireccion.setMaxLength(MAX_LENGTH_DIRECCION);
		txtDireccion.setVisibleLength(VISIBLE_LENGTH_DIRECCION);
		gDatosBasicos.setWidget(1, 0, lDireccion);
		gDatosBasicos.setWidget(1, 1, txtDireccion);
		
		final TextBox txtTipo = new TextBox();
		final Label lTipo = new Label("Tipo: ");
		txtTipo.setMaxLength(MAX_LENGTH_TIPO);
		txtTipo.setVisibleLength(VISIBLE_LENGTH_TIPO);
		gDatosBasicos.setWidget(2, 0, lTipo);
		gDatosBasicos.setWidget(2, 1, txtTipo);

		
		
		
		/* Añadidos -> meterlos como constante
		 	Foto, web, contacto, telefono, web, mail, aforo, tranquilidad, musica en vivo, musica, familiar, romantico, no fumadores
			horario, edades, productoEspecial, precioCaña/refresco/agua/copa/cafe, apropiadoConocerGenteNueva, vistas,
			terraza, climatizado, zonaDeOcio, nacional/extranjero, futbol/basket/.../canalesDePago, tipoAmbiente (pijo, alternativo, descuidado, gay),
			gogo, limpieza, actividadesCulturales, bailar, wifi / ordenadores, parkingPrivado, facilidadAparcar, juegosMesa,
			billar, futbolin, dardos, otrasMaquinas, promocionEspecial (2x1, cada x veces 1 gratis,...), aceptaTarjeta (visa, MasterCard, 4B, ...)
			diasDeMovimiento, precioEntrada, requisitosEspeciales, localEmblemático, familias, empresas, popularidad
		 */

		
		final VerticalPanel vpContacto = new VerticalPanel();
		
		final TextBox telefono = new TextBox();
		vpContacto.add(telefono);
		
			
		
		
		// Creamos el StackPanel para meter los formularios
		final StackPanel stDatosSitio = new StackPanel();
		stDatosSitio.add(gDatosBasicos, "Datos básicos");
		stDatosSitio.add(vpContacto, "Contacto");
		
	    // Make a new list box, adding a few items to it.
/*	    ListBox lb = new ListBox();
	    lb.addItem("foo");
	    lb.addItem("bar");
	    lb.addItem("baz");
	    lb.addItem("toto");
	    lb.addItem("tintin");

	    // Make enough room for all five items (setting this value to 1 turns it
	    // into a drop-down list).
	    lb.setVisibleItemCount(0);

	    // Add it to the root panel.
	    RootPanel.get().add(lb);
	*/	
	
		RootPanel.get().add(stDatosSitio);
//----------------------------------------------------------------------------------------------------------------		
		
		// Focus the cursor on the name field when the app loads
		nameField.setFocus(true);
		nameField.selectAll();

		// Create the popup dialog box
		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText("Remote Procedure Call");
		dialogBox.setAnimationEnabled(true);
		final Button closeButton = new Button("Close");
		// We can set the id of a widget by accessing its Element
		closeButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b>Sending name to the server:</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("<br><b>Server replies:</b>"));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(closeButton);
		dialogBox.setWidget(dialogVPanel);

		// Add a handler to close the DialogBox
		closeButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});

		// Create a handler for the sendButton and nameField
		class MyHandler implements ClickHandler, KeyUpHandler {
			/**
			 * Fired when the user clicks on the sendButton.
			 */
			public void onClick(ClickEvent event) {
				sendNameToServer();
			}

			/**
			 * Fired when the user types in the nameField.
			 */
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					sendNameToServer();
				}
			}

			/**
			 * Send the name from the nameField to the server and wait for a response.
			 */
			private void sendNameToServer() {
				sendButton.setEnabled(false);
				String textToServer = nameField.getText();
				textToServerLabel.setText(textToServer);
				serverResponseLabel.setText("");
				greetingService.greetServer(textToServer,
						new AsyncCallback<String>() {
							public void onFailure(Throwable caught) {
								// Show the RPC error message to the user
								dialogBox
										.setText("Remote Procedure Call - Failure");
								serverResponseLabel
										.addStyleName("serverResponseLabelError");
								serverResponseLabel.setHTML(SERVER_ERROR);
								dialogBox.center();
								closeButton.setFocus(true);
							}

							public void onSuccess(String result) {
								dialogBox.setText("Remote Procedure Call");
								serverResponseLabel
										.removeStyleName("serverResponseLabelError");
								serverResponseLabel.setHTML(result);
								dialogBox.center();
								closeButton.setFocus(true);
							}
						});
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		sendButton.addClickHandler(handler);
		nameField.addKeyUpHandler(handler);
	
	}
}
